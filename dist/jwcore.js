(function ($) 
{
    'use strict';

    var ul_parent = ".ul-parent"
    var a_main = ".a-main"
    var navDropDown = "select.navigation"

    var generic = function(element) {

    }

    generic.prototype.ul_parent = function (e) {
        e.preventDefault()
        $(this).next('ul').fadeToggle("slow", "linear")
    }

    generic.prototype.a_main = function (e) {
        e.preventDefault()
        var obj = {
            pdf:    $(this).data("pdf"),
            href:   $(this).attr("href")
        }
        _activateLink(obj)
    }

    generic.prototype.nav_dropdown = function (e) {
        var url = $(this).val()
        var obj = {
            pdf:    $(this).data("pdf"),
            href:   url
        }
        _activateLink(obj)
    }

    $(document)
        .on('click', ul_parent, generic.prototype.ul_parent)
        .on('click', a_main, generic.prototype.a_main)
        .on('change', navDropDown, generic.prototype.nav_dropdown)
})(jQuery);

function _activateLink(obj) {
    $.log(obj.pdf);
    if (obj.pdf) {
        var html = '<object data="' + obj.href + '?#view=fitH" type="application/pdf" class="inline-pdf">'
        html = html + '<param name="view" value="fitH" />'
        html = html + '<p>It appears you don\'t have a PDF plugin for this browser.</p>'
        html = html + '</object>';
    }
    else {
        var html = $.get(obj.href)
    }

    $(".main-content").empty().html(html)
}
(function ($) {
    $.log = function (value) {
        if (console)
            console.log(value);
    }
    $.log.group = function (value) {
        if (console && console.group)
            console.group(value);
    }
    $.log.groupEnd = function () {
        if (console && console.group)
            console.groupEnd();
    }
    $.log.clear = function () {
        if (console)
            console.clear();
    }
})(jQuery);
(function ($) 
{
    'use strict';
    $("#loadingDiv").hide();
    $('#loadingDiv')
        .hide()  // hide it initially
        .ajaxStart(function (obj, data) {
            $(this).show();
        
        })
        .ajaxStop(function () {
            $(this).hide();
        });
})(jQuery);
$(function () {
    $('.typeahead')
        .each(function (index) {
            var selected = null;
            var settings = {
                prefetchUrl: $(this).data('prefetch-url'),
                remoteUrl: $(this).data('remote-url'),
                wildcard: encodeURI($(this).data('wildcard')),
                displayName: $(this).data('display-name') || 'Value',
                keyName: $(this).data('id-name') || 'Key',
                timeoutMinutes: $(this).data('timeout') || 60,
                hiddenId: $(this).data('hiddenid'),
                minLength: $(this).data('minlength') || 2,
                limit: $(this).data('limit') || 5,
                highlight: ($(this).data('highlight') != false),
                autosubmit: ($(this).data('auto-submit') == true)
            };

            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace(settings.displayName),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function (x) { return x[settings.keyName]; },
                prefetch: (settings.prefetchUrl ? {
                    url: settings.prefetchUrl,
                    ttl: settings.timeoutMinutes * 60 * 1000,
                } : null),
                remote: (settings.remoteUrl ? {
                    url: settings.remoteUrl,
                    wildcard: settings.wildcard
                } : null)
            });

            var options = {
                hint: false,
                highlight: settings.highlight,
                minLength: settings.minLength
            };
            var datasets = {
                display: settings.displayName,
                name: 'bloodhound' + index,
                source: engine,
                limit: settings.limit
            };

            // Wire up typeahead & events
            $(this)
                .typeahead(options, datasets)
                .off('typeahead:select typeahead:autocomplete')
                .on('typeahead:select typeahead:autocomplete', selectTypeahead)
                .off('typeahead:change')
                .on('typeahead:change', changeTypeahead);

            function selectTypeahead(event, suggestion) {
                selected = suggestion;
                changeTypeahead(event, selected[settings.displayName]);
                if (settings.autosubmit)
                {
                    var form = $(this).parents('form');
                    form.submit();
                }
            }

            function changeTypeahead(event, value) {
                if (!settings.hiddenId) return;
                var input = $('#' + settings.hiddenId);
                if (!input) return;
                if (selected && (value === selected[settings.displayName])) {
                    input.val(selected[settings.keyName]);
                }
                else {
                    input.val(null);
                    $(this).val(null);
                }
            }
        })
});