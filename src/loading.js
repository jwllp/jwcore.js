﻿(function ($) 
{
    'use strict';
    $("#loadingDiv").hide();
    $('#loadingDiv')
        .hide()  // hide it initially
        .ajaxStart(function (obj, data) {
            $(this).show();
        
        })
        .ajaxStop(function () {
            $(this).hide();
        });
})(jQuery);