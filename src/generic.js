﻿(function ($) 
{
    'use strict';

    var ul_parent = ".ul-parent"
    var a_main = ".a-main"
    var navDropDown = "select.navigation"

    var generic = function(element) {

    }

    generic.prototype.ul_parent = function (e) {
        e.preventDefault()
        $(this).next('ul').fadeToggle("slow", "linear")
    }

    generic.prototype.a_main = function (e) {
        e.preventDefault()
        var obj = {
            pdf:    $(this).data("pdf"),
            href:   $(this).attr("href")
        }
        _activateLink(obj)
    }

    generic.prototype.nav_dropdown = function (e) {
        var url = $(this).val()
        var obj = {
            pdf:    $(this).data("pdf"),
            href:   url
        }
        _activateLink(obj)
    }

    $(document)
        .on('click', ul_parent, generic.prototype.ul_parent)
        .on('click', a_main, generic.prototype.a_main)
        .on('change', navDropDown, generic.prototype.nav_dropdown)
})(jQuery);

function _activateLink(obj) {
    $.log(obj.pdf);
    if (obj.pdf) {
        var html = '<object data="' + obj.href + '?#view=fitH" type="application/pdf" class="inline-pdf">'
        html = html + '<param name="view" value="fitH" />'
        html = html + '<p>It appears you don\'t have a PDF plugin for this browser.</p>'
        html = html + '</object>';
    }
    else {
        var html = $.get(obj.href)
    }

    $(".main-content").empty().html(html)
}