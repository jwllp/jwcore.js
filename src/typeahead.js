﻿$(function () {
    $('.typeahead')
        .each(function (index) {
            var selected = null;
            var settings = {
                prefetchUrl: $(this).data('prefetch-url'),
                remoteUrl: $(this).data('remote-url'),
                wildcard: encodeURI($(this).data('wildcard')),
                displayName: $(this).data('display-name') || 'Value',
                keyName: $(this).data('id-name') || 'Key',
                timeoutMinutes: $(this).data('timeout') || 60,
                hiddenId: $(this).data('hiddenid'),
                minLength: $(this).data('minlength') || 2,
                limit: $(this).data('limit') || 5,
                highlight: ($(this).data('highlight') != false),
                autosubmit: ($(this).data('auto-submit') == true)
            };

            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace(settings.displayName),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function (x) { return x[settings.keyName]; },
                prefetch: (settings.prefetchUrl ? {
                    url: settings.prefetchUrl,
                    ttl: settings.timeoutMinutes * 60 * 1000,
                } : null),
                remote: (settings.remoteUrl ? {
                    url: settings.remoteUrl,
                    wildcard: settings.wildcard
                } : null)
            });

            var options = {
                hint: false,
                highlight: settings.highlight,
                minLength: settings.minLength
            };
            var datasets = {
                display: settings.displayName,
                name: 'bloodhound' + index,
                source: engine,
                limit: Infinity
            };

            // Wire up typeahead & events
            $(this)
                .typeahead(options, datasets)
                .off('typeahead:select typeahead:autocomplete')
                .on('typeahead:select typeahead:autocomplete', selectTypeahead)
                .off('typeahead:change')
                .on('typeahead:change', changeTypeahead);

            function selectTypeahead(event, suggestion) {
                selected = suggestion;
                changeTypeahead(event, selected[settings.displayName]);
                if (settings.autosubmit)
                {
                    var form = $(this).parents('form');
                    form.submit();
                }
            }

            function changeTypeahead(event, value) {
                if (!settings.hiddenId) return;
                var input = $('#' + settings.hiddenId);
                if (!input) return;
                if (selected && (value === selected[settings.displayName])) {
                    input.val(selected[settings.keyName]);
                }
                else {
                    input.val(null);
                    $(this).val(null);
                }
            }
        })
});
